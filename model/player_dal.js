var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.getAll = function(Team_Name, callback) {
    var query = 'select * from Player where Team_name = ?';
     var queryData = [Team_Name];
    connection.query(query, queryData ,function(err, result) {
        callback(err, result);
    });
};

exports.getAllPlayers = function(callback) {
    var query = 'SELECT * FROM Player ;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getTeams = function(callback) {
    var query = 'Select o.Team_Name, ro.Team_number from Orginization o left join Runs_Orginization ro on ro.Team_Name = o.Team_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(player_id, callback) {
    var query = 'select PS.*, P.* from Player_stats PS ' +
        'right join  Player  P on P.player_id = PS.player_id' +
        ' where P.player_id = ?';
    var queryData = [player_id];
    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO Player (PLayer_first_name, Player_last_name, player_number, Position, Team_name ) VALUES (?,?,?,?,?)';

    var queryData = [ params.PLayer_first_name, params.Player_last_name, params.player_number, params.Position, params.Team_name ];

    connection.query(query, queryData , function(err, result) {

        var player_id = result.insertId;
        callback(err,result);

    });


};

exports.insert_stats = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO Player_stats (player_id, Minutes_per_game, Points_per_game, Assists_per_game, Rebounds_per_game, Blocks_per_game, Steals_per_game, Year_happened, Team ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? )';

    var queryData = [params.player_id, params.Minutes_per_game, params.Points_per_game, params.Assists_per_game,params.Rebounds_per_game, params.Blocks_per_game, params.Steals_per_game, params.Year_happened, params.Team_name ];

    connection.query(query, queryData , function(err, result) {


        callback(err,result);

    });


};



exports.delete = function(player_id, callback) {
    var query = 'Delete FROM Player_stats Where player_id = ?';
    var queryData = [player_id];

    connection.query(query, queryData, function(err, result) {
        var query = ' DELETE FROM Player WHERE player_id = ?';
        var queryData = [player_id];

        connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
    });

};

exports.getResultPPG = function (points, callback) {
    var query = 'SELECT player_id,PLayer_first_name, Player_last_name  FROM Player WHERE EXISTS (SELECT Points_per_game FROM Player_stats WHERE player_id = Player.player_id and Points_per_game >= ?) Group by player_id';

    var queryData = [points];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getResultAPG = function (assits, callback) {
    var query = 'SELECT player_id,PLayer_first_name, Player_last_name  FROM Player WHERE EXISTS (SELECT Assists_per_game FROM Player_stats WHERE player_id = Player.player_id and Assists_per_game >= ?) Group by player_id';

    var queryData = [assits];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getResultRPG = function (assits, callback) {
    var query = 'SELECT player_id,PLayer_first_name, Player_last_name  FROM Player WHERE EXISTS (SELECT Rebounds_per_game FROM Player_stats WHERE player_id = Player.player_id and Rebounds_per_game >= ?) Group by player_id';

    var queryData = [assits];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE Player SET PLayer_first_name= ?, Player_last_name= ?, player_number = ?, Position = ?, Team_name = ? WHERE player_id = ?';

    var queryData = [ params.PLayer_first_name, params.Player_last_name, params.player_number, params.Position, params.Team_Name, params.player_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.delete_stats = function(params, callback) {
    var query = 'DELETE FROM Player_stats WHERE player_id = ? AND Minutes_per_game= ? AND Points_per_game = ? AND Assists_per_game = ? AND Rebounds_per_game = ? AND Blocks_per_game = ? AND Steals_per_game = ? AND Year_happened= ? AND Team = ?';

    var queryData = [params.player_id, params.Minutes_per_game, params.Points_per_game, params.Assists_per_game,params.Rebounds_per_game, params.Blocks_per_game, params.Steals_per_game, params.Year_happened, params.Team_name ];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};