var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM Orginization ;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(Team_Name, callback) {
    var query = 'drop View if exists Orginization_info';
    var queryData = [Team_Name];

    connection.query(query, function(err, result){
            var query_2 = ' CREATE VIEW Orginization_info as select Owner_first_name, Owner_last_name, Manager_first_name, Manager_last_name,Coach_first_name, Coach_last_name, o.Team_Name, o.City, o.State  FROM Front_office fo ' +
            'right join Runs_Orginization ro on ro.Team_number = fo.team_number' +
            ' right join Orginization o on o.Team_name =ro.Team_name where o.Team_name = ?';


                 connection.query(query_2, queryData, function(err, result) {
                     var query = 'select * From Orginization_info';
                     connection.query(query, function(err, result){
                         callback(err, result);
                });
            });
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO Orginization (Team_Name, Division, Conferance, City , State ) VALUES (?,?,?,?,?)';

    var queryData = [params.Team_Name, params.Division, params.Conferance, params.City, params.State];

    connection.query(query, queryData , function(err, result) {
        var Team_Name= params.Team_Name;
        var query = 'INSERT INTO Front_office (Owner_first_name, Owner_last_name, Manager_first_name, Manager_last_name,Coach_first_name, Coach_last_name) VALUES (?,?,?,?,?,?) ';
        var queryData = [params.Owner_first_name, params.Owner_last_name,params.Manager_first_name, params.Manager_last_name, params.Coach_first_name, params.Coach_last_name];
        connection.query(query, queryData , function(err, result) {
            var team_number = result.insertId;
            var query = 'INSERT INTO Runs_Orginization(Team_number, Team_name) VALUES (?,?)';
            var queryData = [ team_number,Team_Name];
            connection.query(query, queryData , function(err, result) {
             callback(err,result);
        });
        });

    });

};


exports.delete = function(Team_Name, callback) {
    var query = 'Delete fo from Front_office fo left join Runs_Orginization ro on ro.Team_number =fo.team_number where ro.Team_name =  ?';
    var queryData = [Team_Name];

    connection.query(query, queryData, function(err, result) {

        var query = 'DELETE FROM Orginization WHERE Team_Name = ?';
        var queryData = [Team_Name];
        connection.query(query, queryData, function(err,result) {
        callback(err, result);
    });
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE Orginization SET City= ?, State =? WHERE Team_name = ?';

    var queryData = [ params.City, params.State, params.Team_Name];

    connection.query(query, queryData, function(err, result) {
        var query = 'UPDATE Front_office SET ' +
            'Owner_first_name = ?, Owner_last_name = ?, ' +
            'Manager_first_name = ?, Manager_last_name =?,' +
            ' Coach_first_name = ?, Coach_last_name = ?' +
            ' WHERE team_number = ?';
        var queryData = [params.Owner_first_name, params.Owner_last_name, params.Manager_first_name, params.Manager_last_name , params.Coach_first_name, params.Coach_last_name, params.team_number];
        connection.query(query, queryData, function(err, result) {
            callback(err, result);
        });
    });
};


exports.edit = function(Team_name, callback) {
    var query = 'CALL OrginizationGetInfo (?)';
    var queryData = [Team_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
exports.getsumppg = function (number_req, callback) {
    var query = 'select o.Team_Name, sum(ps.Points_per_game) As Statring_5_Ppg From  Player_stats ps ' +
        'join Player p  on ps.player_id=p.player_id ' +
        'join Orginization o on p.Team_name=o.Team_Name ' +
        'GROUP BY Team ' +
        'Having sum(ps.Points_per_game) > (?)';

    var queryData = [number_req];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

